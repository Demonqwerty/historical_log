  <!-- resources/views/tasks.blade.php -->



<?php $__env->startSection('content'); ?>

  <!-- Bootstrap шаблон... -->

  <div class="panel-body">
    <!-- Отображение ошибок проверки ввода -->
    <?php echo $__env->make('common.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Форма новой задачи -->
    <form action="<?php echo e(url('user')); ?>" method="POST" class="form-horizontal">
      <?php echo e(csrf_field()); ?>




  <!-- TODO: Текущие задачи -->
  <?php if(count($users) > 0): ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        Текущая задача
      </div>

      <div class="panel-body">
        <table class="table table-striped task-table">

          <!-- Заголовок таблицы -->
          <thead>
          <th>Task</th>
          <th>&nbsp;</th>
          </thead>

          <!-- Тело таблицы -->
          <tbody>
          <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <!-- Имя задачи -->
              <td class="table-text">
                <div><?php echo e($user->name); ?></div>
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>