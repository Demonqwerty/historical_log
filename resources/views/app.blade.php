<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 24.10.18
 * Time: 12:13
 */<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
    <head>
        <title>App Name - @yield('title')</title>
</head>
<body>
@section('sidebar')
    This is the master sidebar.
@show

<div class="container">
    @yield('content')
</div>
</body>
</html>