<!DOCTYPE html>
<html>
<head>
    <title>Device {{ $device->id }}</title>
</head>
<body>
<h1>Device {{ $device->id }}</h1>
<ul>
    <li>Make: {{ $device->make }}</li>
    <li>Model: {{ $device->model }}</li>
    <li>Produced on: {{ $device->produced_on }}</li>
</ul>
</body>
</html>