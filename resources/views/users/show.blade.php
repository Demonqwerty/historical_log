
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.10.18
 * Time: 17:38
 */
<html>
  <head>
    <title>Car {{ $user->id }}</title>
</head>
<body>
<h1>Car {{ $user->id }}</h1>
<ul>
    <li>Make: {{ $user->make }}</li>
    <li>Model: {{ $user->model }}</li>
    <li>Produced on: {{ $user->produced_on }}</li>
</ul>
</body>
</html>