  <!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')

  <!-- Bootstrap шаблон... -->

  <div class="panel-body">
    <!-- Отображение ошибок проверки ввода -->
    @include('common.errors')

    <!-- Форма новой задачи -->
    <form action="{{ url('user') }}" method="POST" class="form-horizontal">
      {{ csrf_field() }}



  <!-- TODO: Текущие задачи -->
  @if (count($users) > 0)
    <div class="panel panel-default">
      <div class="panel-heading">
        Текущая задача
      </div>

      <div class="panel-body">
        <table class="table table-striped task-table">

          <!-- Заголовок таблицы -->
          <thead>
          <th>Task</th>
          <th>&nbsp;</th>
          </thead>

          <!-- Тело таблицы -->
          <tbody>
          @foreach ($users as $user)
            <tr>
              <!-- Имя задачи -->
              <td class="table-text">
                <div>{{ $user->name }}</div>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  @endif
@endsection
